% author: Carolina de Senne Garcia
% ra 145656
% desennecarol@gmail.com

% load this file in swipl
% [exercicios].

% tamanho de uma lista
tam([],0).
tam([_|R],N) :- tam(R,NN), N is NN+1.

% tamanho com acumulador
tam_acc(L,N) :- tamx(L,N,0).
tamx([],N,Acc) :- Acc=N.
tamx([_|R],N,Acc) :- AA is Acc+1,tamx(R,N,AA).

% append duas listas
append_lists([],A,A).
append_lists([X|R],A,AA) :- append_lists(R,A,RR), AA = [X|RR].

% soma dos elementos de uma lista soma(+LISTA,-SOMA)
soma(L,S) :- somacc(L,S,0).
somacc([],Acc,Acc).
somacc([X|R],S,Acc) :- NAcc is Acc+X, somacc(R,S,NAcc).

% soma dos números pares de uma lista somap(+LISTA,-SOMA)
somap([],0).
somap([X|R],N) :- X mod 2 =:= 0, somap(R,NN), N is NN+X.
somap([_|R],N) :- somap(R,N).

% soma dos elementos nas posições pares da lista ( o primeiro elemento esta na posicao 1) somapares(+LISTA,-SOMA)
somapospar([],0).
somapospar([_|R],S) :- somaposimpar(R,S).

% soma dos elementos nas posicoes impares da lista
somaposimpar([],0).
somaposimpar([X|R],S) :- somapospar(R,SS), S is SS+X.

% existe item na lista elem(+IT,+LISTA)
existe(X,[X|_]).
existe(X,[_|R]) :- existe(X,R).

% posição do item na lista: 1 se é o primeiro, falha se nao esta na lista pos(+IT,+LISTA,-POS)
posicao(X,[X|_],1).
posicao(X,[_|R],P) :- posicao(X,R,PP), P is PP+1.

% conta quantas vezes o item aparece na lista (0 se nenhuma) conta(+IT,+LISTA,-CONTA)
conta(_,[],0).
conta(X,[X|R],C) :- conta(X,R,CC), C is CC+1.
conta(X,[_|R],C) :- conta(X,R,C).

% maior elemento de uma lista - maior(+LISTA,-MAX)
maior([X],X).
maior([X|R],M) :- maior(R,MM), (X > MM -> M=X; M=MM).

% myappend elemento em uma lista (+LISTA,+ELEMENTO,-RESULTADO)
myappend(Y, [], [Y]).
myappend(Y, [X|R], [X|Z]) :- myappend(Y,R,Z).

% reverte uma lista
reverse([],[]).
reverse([X|R],Z) :- reverse(R,Y), myappend(X,Y,Z).

% intercala 2 listas (intercala1 e intercala2)
intercala([A|RA], [B|RB], [A,B|R]) :- !, intercala(RA, RB, R).
intercala([], B, B).
intercala(A, [], A).

% a lista ja esta ordenada (isSorted)
isSorted([]).
isSorted([_]).
isSorted([X,Y|R]) :- X =< Y, isSorted([Y|R]). % > fix coloring in sublime.

% dado n gera a lista de 1 a n (+N,-LISTA)
gera(0,[]).
gera(N,L) :- gera(NN,LL), myappend(N,LL,L), N is NN+1.

% retorna o ultimo elemento de uma lista (+LISTA,-ULTIMO)
ultimo([X],X).
ultimo([_|R],U) :- ultimo(R,U).

% retorna a lista sem o utlimo elemento (+LISTA,-LISTA_NOVA)
remove_ultimo([_],[]).
remove_ultimo([X|R],[X|U]) :- remove_ultimo(R,U).

% shift right
shiftr([],[]).
shiftr(L,[U|RU]) :- ultimo(L,U),remove_ultimo(L,RU).

% shiftr n lista (shift right n vezes)
shiftr_n([],_,[]).
shiftr_n(L,0,L).
shiftr_n(L,N,LR) :- shiftr(L,L1), N1 is N-1, shiftr_n(L1,N1,LR).

% shift left
shiftleft([],[]).
shiftleft([X|R],RR) :- myappend(X,R,RR).

% shift left n vezes
shiftleft_n([],_,[]).
shiftleft_n(L,0,L).
shiftleft_n(L,N,LR) :- shiftleft(L,L1), N1 is N-1, shiftleft_n(L1,N1,LR).

% remove item da lista (1 vez so) (+ELEMENTO,+LISTA,-LISTA_NOVA)
remove1(_,[],[]).
remove1(X,[X|R],R).
remove1(X,[Y|R],[Y|RR]) :- remove1(X,R,RR).

% remove item da lista (todas as vezes)
remove(_,[],[]).
remove(X,[X|R],L) :- remove(X,R,L).
remove(X,[Y|R],[Y|RR]) :- remove(X,R,RR).

% remove item da lista n (as primeiras n vezes)
remove_n(_,[],_,[]).
remove_n(_,L,0,L).
remove_n(X,[X|R],N,L) :- N1 is N-1, remove_n(X,R,N1,L).
remove_n(X,[Y|R],N,[Y|L]) :- remove_n(X,R,N,L).

% remove item da lista (a ultima vez que ele aparece)
remove_ultimo_e(X,L,R) :- reverse(L,L1), remove1(X,L1,L2), reverse(L2,R).

% troca velho por novo na lista (1 so vez)
troca1([],_,_,[]).
troca1([X|R],N,V,LN) :- X=V, LN=[N|R].
troca1([X|R],N,V,LN) :- troca1(R,N,V,LLNN), LN=[X|LLNN].

% troca velho por novo na lista (todas vezes)
troca([],_,_,[]).
troca([V|R],N,V,[N|LN]) :- troca(R,N,V,LN).
troca([X|R],N,V,[X|LN]) :- troca(R,N,V,LN).

% troca velho por novo na lista k (as primeiras k vezes)
troca_n([],_,_,_,[]).
troca_n(L,_,_,0,L).
troca_n([V|R],N,V,K,[N|LN]) :- K1 is K-1, troca_n(R,N,V,K1,LN).
troca_n([X|R],N,V,K,[X|LN]) :- troca_n(R,N,V,K,LN).

% maior com if-else de flecha
ma([X],X).
ma([X|R],M) :- ma(R,MM), (MM>X -> M=MM; M=X).

% maior com outra sintaxe
mx([X],X).
mx([X|R],M) :- mx(R,MM), MM > X, M=MM.
mx([X|_],M) :- M=X.

% nota conceitual
nota(N,L) :- N>9,!, L=a.
nota(N,L) :- N>7,!, L=b.
nota(N,L) :- N>5,!, L=c.
nota(_,d).

% elemento esta na lista (com cut !)
elem(_,[]) :- !,fail.
elem(I,[I|_]).
elem(I,[_|R]) :- elem(I,R).

%%%%%%%%%%%%%%%%%%%%%%%%%% ARVORES %%%%%%%%%%%%%%%%%%%%%%%%%%

% acha um item em uma arvore de busca binaria
isInTree(E,arv(E,_,_)) :- !.
isInTree(E,arv(X,L,R)) :- E < X 
						  -> isInTree(E,L)
						  ; isInTree(E,R).

% verifica se uma arvore é um abb
isBST(empty) :- !.
isBST(arv(_,empty,empty)) :- !.
isBST(arv(A,arv(B,BL,BR),empty)) :- !, A > B, isBST(arv(B,BL,BR)).
isBST(arv(A,empty,arv(C,CL,CR))) :- !, C > A, isBST(arv(C,CL,CR)).
isBST(arv(A,arv(B,BL,BR),arv(C,CL,CR))) :- A > B, C > A, isBST(arv(B,BL,BR)), isBST(arv(C,CL,CR)).

% insere um item numa abb (+E,+T,-T)
insertBST(E,empty,arv(E,empty,empty)) :- !.
insertBST(E,arv(E,L,R),arv(E,L,R)) :- !.
insertBST(E,arv(X,L,R),arv(X,NT,R)) :- X > E, !, insertBST(E,L,NT).
insertBST(E,arv(X,L,R),arv(X,L,NT)) :- X < E, !, insertBST(E,R,NT).

% remove um item de uma abb
% TODO AULA

% calcula a profundidade maxima de uma abb (+T,-D)
depth(empty,0).
depth(arv(_,L,R),D) :- depth(L,DL), depth(R,DR), D is max(DL,DR)+1.

% converte uma abb numa lista em ordem infixa (arvore-esquerda, no, arvore-direita)
infixBST(empty,[]).
infixBST(arv(X,L,R),List) :- infixBST(L,ListL), infixBST(R,ListR), append(ListL,[X|ListR],List).

% converte uma abb numa lista em ordem prefixa (no, ae, ad)
prefixBST(empty,[]).
prefixBST(arv(X,L,R),List) :- prefixBST(L,ListL), prefixBST(R,ListR), append([X|ListL],ListR,List).

% converte uma lista em uma abb (+L,-T)
listToBST([],empty).
listToBST([X|R],T) :- listToBST(R,TT), insertBST(X,TT,T).

%%%%%%%%%%%%%%%%%%%%%%%% DICIONARIOS %%%%%%%%%%%%%%%%%%%%%%%%

% dado um dicionário acesse o valor associado a uma chave, falha se a chave não esta no dicionário
access(Dic,K,V) :- append(_,[(K,V)|_],Dic).

% insere um par chave valor no dicionário (ou troca o valor associado a chave se ela ja esta no dicionário)
insertDic([],K,V,[(K,V)]) :- !.
insertDic([(K,_)|R],K,V,[(K,V)|R]) :- !.
insertDic([X|R],K,V,[X|RR]) :- insertDic(R,K,V,RR).

% remove uma chave (e seu valor) do dicionário.
removeDic([],_,[]) :- !.
removeDic([(K,_)|R],K,R) :- !.
removeDic([X|R],K,[X|RR]) :- removeDic(R,K,RR).

% contador: dicionario onde valor associado é um inteiro. 
% Implemente o soma1 que dado um contador soma um ao valor associado a uma chave, 
% ou se ela nao estiver no dicionario, acrescenta a chave com valor 1.
soma1Dic([],K,[(K,1)]) :- !.
soma1Dic([(K,V)|R],K,[(K,VV)|R]) :- !,VV is V+1.
soma1Dic([X|R],K,[X|RR]) :- soma1Dic(R,K,RR).

