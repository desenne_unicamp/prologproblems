% author: Carolina de Senne Garcia
% RA 145656
% desennecarol@gmail.com

% load this file in swipl
% [family].

% Relacoes familiares

% dados

pai(antonio, carlos).
pai(antonio, ana).
pai(carlos, pedro).
pai(pedro, luiza).
pai(pedro, joao).

mae(carol, carlos).
mae(carol, ana).
mae(ana, daniel).
mae(julia, joao).
mae(julia, luiza).
mae(julia, roberto).

masculino(antonio).
masculino(carlos).
masculino(joao).
masculino(roberto).
masculino(pedro).
masculino(daniel).

feminino(carol).
feminino(julia).
feminino(ana).
feminino(luiza).

% definicoes

irmao(X,Y) :- pai(P,X), pai(P,Y), mae(M,X), mae(M,Y), X \= Y.
meioirmao(X,Y) :- (pai(P,X), pai(P,Y) ; mae(M,X), mae(M,Y)), X \= Y.

antecessor(A,X) :- pai(A,X).
antecessor(A,X) :- pai(A,Y) , antecessor(Y,X).

erropai(X) :- pai(X,_),feminino(X).
erromae(X) :- mae(X,_),masculino(X).

tio(X,Y) :- masculino(X), (irmao(X,P),pai(P,Y) ; irmao(X,M),mae(M,Y)).
tia(X,Y) :- feminino(X), (irmao(X,P),pai(P,Y) ; irmao(X,M),mae(M,Y)).

