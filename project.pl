% author: Carolina de Senne Garcia
% RA 145656
% c145656@dac.unicamp.br
% desennecarol@gmail.com

% Descritpion: https://www.ic.unicamp.br/~wainer/cursos/2s2019/346/proj-prolog.html

% DUVIDAS:
% verificar se saida esta OK
% verificar se algoritmo OK (nao usei findAll, so usei uma lista)
% prof tem testes?
% verificar se intercessao quadrado circulo esta correta
% verificar findIntercessions

topo() :- read(Input), findAllIntercessions(Input,Output), print_size(Output), print_each_line(Output).

% imprime o tamanho da lista de saida
print_size(L) :- put(10), size(L,Size), write(Size).

% size retorna o tamanho de uma lista
size([],0).
size([_|R],N) :- size(R,NN), N is NN+1.

% imprime cada linha da saida
print_each_line([]) :- !,put(10).
print_each_line([E|R]) :- put(10), write(E), print_each_line(R).

% findAllIntercessions encontra todos os pares de figuras que possuem intercessao (+FIGURAS,-PARES)
% acha todas as intercessoes entre o elemento presente e o resto da lista (evitar repeticoes)
findAllIntercessions([],[]).
findAllIntercessions([E|R],I) :- findIntercessions(E,R,EInter), findAllIntercessions(R,RInter), append(EInter,RInter,I).

% findIntercessions encontra todas as intercessoes entre uma figura e uma lista de figuras (+FIGURA,+LISTA,-PARES)
findIntercessions(_,[],[]).
findIntercessions(circ(N1,X1,Y1,R1),[circ(N2,X2,Y2,R2)|R],[(N1,N2)|RR]) :- intercessaoCircCirc(circ(N1,X1,Y1,R1),circ(N2,X2,Y2,R2)),!,
                                                                           findIntercessions(circ(N1,X1,Y1,R1),R,RR).
findIntercessions(circ(N1,X1,Y1,R1),[_|R],RR) :- findIntercessions(circ(N1,X1,Y1,R1),R,RR).
findIntercessions(quad(N1,X1,Y1,R1),[quad(N2,X2,Y2,R2)|R],[(N1,N2)|RR]) :- intercessaoQuadQuad(quad(N1,X1,Y1,R1),quad(N2,X2,Y2,R2)),!,
                                                                           findIntercessions(quad(N1,X1,Y1,R1),R,RR).
findIntercessions(quad(N1,X1,Y1,R1),[_|R],RR) :- findIntercessions(quad(N1,X1,Y1,R1),R,RR).
findIntercessions(quad(N1,X1,Y1,R1),[circ(N2,X2,Y2,R2)|R],[(N1,N2)|RR]) :- intercessaoQuadCirc(quad(N1,X1,Y1,R1),circ(N2,X2,Y2,R2)),!,
                                                                           findIntercessions(quad(N1,X1,Y1,R1),R,RR).
findIntercessions(quad(N1,X1,Y1,R1),[_|R],RR) :- findIntercessions(quad(N1,X1,Y1,R1),R,RR).
findIntercessions(circ(N1,X1,Y1,R1),[quad(N2,X2,Y2,R2)|R],[(N1,N2)|RR]) :- intercessaoQuadCirc(quad(N2,X2,Y2,R2),circ(N1,X1,Y1,R1)),!,
                                                                           findIntercessions(circ(N1,X1,Y1,R1),R,RR).
findIntercessions(circ(N1,X1,Y1,R1),[_|R],RR) :- findIntercessions(quad(N1,X1,Y1,R1),R,RR).

% distancia entre dois pontos (X1,Y1) e (X2,Y2)
dist(X1,Y1,X2,Y2,D) :- D is sqrt((X2-X1)**2 + (Y2-Y1)**2).
% verifica se um ponto esta contido em um circulo
pointIsInCircle(PX,PY,CX,CY,R) :- dist(PX,PY,CX,CY,D), R >= D.
% acha o ponto bottom-right de um quadrado de lado L e centro (X,Y)
bottomRight(X,Y,L,BRX,BRY) :- BRX is X+(L/2), BRY is Y-(L/2).
% acha o ponto bottom-left de um quadrado de lado L e centro (X,Y)
bottomLeft(X,Y,L,BLX,BLY) :- BLX is X-(L/2), BLY is Y-(L/2).
% acha o ponto top-right de um quadrado de lado L e centro (X,Y)
topRight(X,Y,L,TRX,TRY) :- TRX is X+(L/2), TRY is Y+(L/2).
% acha o ponto top-left de um quadrado de lado L e centro (X,Y)
topLeft(X,Y,L,TLX,TLY) :- TLX is X-(L/2), TLY is Y+(L/2).

% define a intercessao entre dois circulos
intercessaoCircCirc(circ(_,X1,Y1,R1),circ(_,X2,Y2,R2)) :- SomaRaios is R1+R2, 
                                                          dist(X1,Y1,X2,Y2,DistCentros),
                                                          SomaRaios >= DistCentros.
% define a intercessao entre dois quadrados
intercessaoQuadQuad(quad(_,X1,Y1,L1),quad(_,X2,Y2,L2)) :- SomaLados is (L1/2)+(L2/2),
                                                          DistX is abs(X2-X1), DistY is abs(Y2-Y1),
                                                          SomaLados >= DistX, SomaLados >= DistY.
% define a intercessao entre um quadrado e um circulo
intercessaoQuadCirc(quad(_,X1,Y1,L),circ(_,X2,Y2,R)) :- SomaMedidas is (L/2)+R,
                                                        DistX is abs(X2-X1), DistY is abs(Y2-Y1),
                                                        SomaMedidas >= DistX, SomaMedidas >= DistY, !.
intercessaoQuadCirc(quad(_,X1,Y1,L),circ(_,X2,Y2,R)) :- bottomRight(X1,Y1,L,PX,PY), pointIsInCircle(PX,PY,X2,Y2,R),!.
intercessaoQuadCirc(quad(_,X1,Y1,L),circ(_,X2,Y2,R)) :- bottomLeft(X1,Y1,L,PX,PY), pointIsInCircle(PX,PY,X2,Y2,R),!.
intercessaoQuadCirc(quad(_,X1,Y1,L),circ(_,X2,Y2,R)) :- topRight(X1,Y1,L,PX,PY), pointIsInCircle(PX,PY,X2,Y2,R),!.
intercessaoQuadCirc(quad(_,X1,Y1,L),circ(_,X2,Y2,R)) :- topLeft(X1,Y1,L,PX,PY), pointIsInCircle(PX,PY,X2,Y2,R),!.
